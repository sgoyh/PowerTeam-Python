﻿# -*- coding: utf-8 -*-

#版本信息
VERSION = '1.0'

#是否开启调试
DEBUG = True

#绑定的IP或域名,发送邮件中的系统链接使用该配置
HOST = '127.0.0.1'

#绑定的端口
PORT = 80

#Cookie密钥
SECRET_KEY = 'PowerTeamSecret'

#每页显示的数据条数
PAGESIZE = 10

#编辑器上传文件路径
import os
UPLOADDIR = 'static/upload/'
UPLOADPATH = os.path.join(os.path.dirname(__file__),UPLOADDIR)

#管理员重设密码的默认密码
DEFAULT_PASSWORD = '123'

#是否开启邮件通知服务 True(开启) False(关闭)
ENABLE_MAIL_NOTICE = False
#发送邮件的服务器
SMTP_HOST = 'smtp.qq.com'
#发送邮件的用户名
SMTP_USER = 'test@qq.com'
#发送邮件的密码
SMTP_PASS = 'test'

#数据库链接
DB = 'sqlite:///PowerTeam.db'

#'mysql://username:password@host:port/database'

#BAE
#from bae.core import const
#DB = 'mysql://'+const.MYSQL_USER+':'+const.MYSQL_PASS+'@'+const.MYSQL_HOST+':'+const.MYSQL_PORT+'/database' 

#SAE
#import sae.const
#DB = 'mysql://'+sae.const.MYSQL_USER+':'+sae.const.MYSQL_PASS+'@'+sae.const.MYSQL_HOST+':'+sae.const.MYSQL_PORT+'/'+sae.const.MYSQL_DB