﻿# -*- coding: utf-8 -*-

from flask import Flask
from powerteam.controllers import *

def create_powerteam_app():
    app = Flask(__name__)
    app.config.from_pyfile('powerteamconfig.py')
    #解决angularjs的模版冲突
    app.jinja_env.variable_start_string = '(('
    app.jinja_env.variable_end_string = '))'
    app.register_module(home)
    app.register_module(project)
    app.register_module(task)
    app.register_module(team)
    app.register_module(issue)
    app.register_module(upload)
    app.register_module(user)
    app.register_module(admin)
    return app