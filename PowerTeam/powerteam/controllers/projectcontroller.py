﻿# -*- coding: utf-8 -*-

from flask import Module,render_template,jsonify,redirect,request,g,session
from powerteam.services import projectservice, taskservice, issueservice, teamservice,userservice
from powerteam.controllers.filters import login_filter

project = Module(__name__)
project.before_request(login_filter)

@project.route('/Project')
def index():
    return render_template('Project/List.html')

@project.route('/Project/Query',methods=['POST'])
def query():
    project_name = request.json['ProjectName']
    status = request.json['Status']
    page_no = request.json['PageNo']
    (row_count,page_count,page_no,page_size,data) = projectservice.query(project_name,status,page_no,'CreateDate desc',g.user_id)
    projects = []
    for p in data.all():
        projects.append({'ProjectId':p.ProjectId,'ProjectName':p.ProjectName,'Status':p.Status,'Progress':p.Progress,'CreateDate':p.CreateDate.isoformat(),'Creator':p.UserProfile.Nick})
    return jsonify(row_count=row_count,page_count=page_count,page_no=page_no,page_size=page_size,data=projects)

@project.route('/Project/Create',methods=['POST'])
def create():
    projectservice.create(request.json['ProjectName'],g.user_id)
    return jsonify(created=True)

@project.route('/Project/Delete',methods=['POST'])
def delete():
    projectservice.delete(request.json['ProjectId'])
    return jsonify(deleted=True)

@project.route('/Project/Detail/<int:project_id>')
def detail(project_id):
    project = projectservice.get(project_id)

    return render_template('Project/Detail.html',Project=project,CreatorName=project.UserProfile.Nick,CurrentUser=g.user_id)

@project.route('/Project/Update',methods=['POST'])
def update():
    project_id = request.json['ProjectId']
    project_name = request.json['ProjectName']
    status = request.json['Status']
    projectservice.update(project_id,project_name,status)
    return jsonify(updated=True)

@project.route('/Project/Dashboard/<int:project_id>')
def dashboard(project_id):
    (task_status,task_priority) = taskservice.statistics(project_id)
    (issue_status,issue_priority) = issueservice.statistics(project_id)
    project = projectservice.get(project_id)
    member_list = teamservice.member_in_project(project_id)
    return render_template('Project/Dashboard.html',Project=project,TaskStatus=task_status,TaskPriority=task_priority,IssueStatus=issue_status,IssuePriority=issue_priority,MemberList=member_list)