﻿# -*- coding: utf-8 -*-

from powerteam.controllers.homecontroller import home
from powerteam.controllers.projectcontroller import project
from powerteam.controllers.taskcontroller import task
from powerteam.controllers.teamcontroller import team
from powerteam.controllers.issuecontroller import issue
from powerteam.controllers.uploadcontroller import upload
from powerteam.controllers.usercontroller import user
from powerteam.controllers.admincontroller import admin